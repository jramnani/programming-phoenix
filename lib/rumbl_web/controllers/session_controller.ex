defmodule RumblWeb.SessionController do
	use RumblWeb, :controller

  alias Rumbl.Accounts

  def new(conn, _) do
    render(conn, "new.html", conn: conn)
  end

  def create(conn, %{
        "session" => %{
                 "username" => username,
                 "password" => password
               }
             }) do
    case Accounts.authenticate_user(username, password) do
      {:ok, user} ->
        conn
        |> RumblWeb.Auth.login(user)
        |> put_flash(:info, "Welcome back, #{user.name}!")
        |> redirect(to: ~p"/")

      {:error, _reason} ->
        conn
        |> put_flash(:error, "Invalid username/password combination")
        |> render("new.html")
    end
  end

  # def create(conn, _params) do
  #   conn
  #   |> put_flash(:error, "Invalid username/password combination")
  #   |> render("new.html")
  # end

  def delete(conn, _params) do
    conn
    |> RumblWeb.Auth.logout()
    |> redirect(to: ~p"/")
  end
end
