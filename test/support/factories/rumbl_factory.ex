defmodule Rumbl.Factory do
  use ExMachina.Ecto, repo: Rumbl.Repo

  def user_factory do
    %Rumbl.Accounts.User{
      name: "Test Name",
      username: sequence("username"),
      password: "secretpass"
    }
  end

  def video_factory do
    title = sequence(:title, &"Unit Test Video - Part #{&1}")
    %Rumbl.Multimedia.Video{
      title: title,
      description: "A description of the video.",
      url: "https://example.com/?v=abc123",
      user: build(:user)
      # By default, Ecto does not load associations.
      # There's a mismatch between me wanting the factory to create a record
      # with all its required foreign key dependencies, but the default
      # implementations of functions like Multimedia.list_videos() does not
      # preload the User relationship. The compromise for now is to manually
      # specify an association that is not preloaded.
      # user: %Ecto.Association.NotLoaded{
      #   __owner__: Rumbl.Multimedia.Video,
      #   __field__: :user,
      #   __cardinality__: :one
      # }
    }
  end
end
