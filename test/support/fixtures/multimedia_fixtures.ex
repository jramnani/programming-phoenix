defmodule Rumbl.MultimediaFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Rumbl.Multimedia` context.
  """

  @doc """
  Generate a video.
  """
  def video_fixture(attrs \\ %{}) do
    {:ok, user} =
      %{
        username: "videouser",
        name: "Video User",
        password: "secretpass"
      }
      |> Rumbl.Accounts.create_user()

    video_attrs =
      attrs
      |> Enum.into(%{
        description: "some description",
        title: "some title",
        url: "some url"
      })

    {:ok, video} = Rumbl.Multimedia.create_video(user, video_attrs)

    video
  end
end
