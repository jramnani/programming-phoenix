defmodule Rumbl.MultimediaTest do
  use Rumbl.DataCase, async: true

  alias Rumbl.Multimedia

  describe "categories" do
    alias Rumbl.Multimedia.Category

    test "creates a category" do
      result = Multimedia.create_category!("foo")

      assert %Category{} = result
    end

    test "upserts when trying to create a duplicate" do
      first = Multimedia.create_category!("test")
      # This should not raise a DB constraint exception
      second = Multimedia.create_category!("test")

      assert first.id != nil
      # Repo.insert!(on_conflict: :nothing) returns nil as the ID if the record
      # already exists.
      assert second.id == nil
    end

    test "can list categories alphabetically" do
      second = Multimedia.create_category!("xyz")
      first = Multimedia.create_category!("abc")

      results = Multimedia.list_alphabetical_categories()

      assert [first, second] == results
    end
  end

  describe "videos" do
    alias Rumbl.Multimedia.Video

    @invalid_attrs %{description: nil, title: nil, url: nil}

    test "list_videos/0 returns all videos" do
      video = Factory.insert(:video) |> Ecto.reset_fields([:user])

      assert Multimedia.list_videos() == [video]
    end

    test "get_video!/1 returns the video with given id" do
      video = Factory.insert(:video) |> Ecto.reset_fields([:user])
      assert Multimedia.get_video!(video.id) == video
    end

    test "create_video/2 with valid data creates a video" do
      user = Factory.insert(:user)
      valid_attrs = %{description: "some description", title: "Some Title", url: "https://example.com/?v=abc123"}

      assert {:ok, %Video{} = video} = Multimedia.create_video(user, valid_attrs)
      assert video.description == "some description"
      assert video.title == "Some Title"
      assert video.url == "https://example.com/?v=abc123"
      assert video.slug == "some-title"
      assert video.user.id == user.id
    end

    test "create_video/2 with invalid data returns error changeset" do
      user = Factory.insert(:user)
      assert {:error, %Ecto.Changeset{}} = Multimedia.create_video(user, @invalid_attrs)
    end

    test "update_video/2 with valid data updates the video" do
      user = Factory.insert(:user)
      video = Factory.insert(:video, user: user)

      update_attrs = %{
        description: "some updated description",
        title: "some updated title",
        url: "some updated url"
      }

      assert {:ok, %Video{} = video} = Multimedia.update_video(video, update_attrs)
      assert video.description == "some updated description"
      assert video.title == "some updated title"
      assert video.url == "some updated url"
    end

    test "update_video/2 with invalid data returns error changeset" do
      # Use Ecto.reset_fields to prevent Ecto queries not preloading
      # associations while ExMachina factories do.
      # https://github.com/thoughtbot/ex_machina/issues/295
      video = Factory.insert(:video) |> Ecto.reset_fields([:user])
      assert {:error, %Ecto.Changeset{}} = Multimedia.update_video(video, @invalid_attrs)
      assert video == Multimedia.get_video!(video.id)
    end

    test "delete_video/1 deletes the video" do
      user = Factory.insert(:user)
      video = Factory.insert(:video, user: user)

      assert {:ok, %Video{}} = Multimedia.delete_video(video)
      assert_raise Ecto.NoResultsError, fn -> Multimedia.get_video!(video.id) end
    end

    test "change_video/1 returns a video changeset" do
      video = Factory.insert(:video)
      assert %Ecto.Changeset{} = Multimedia.change_video(video)
    end

    test "list_user_videos returns empty list when no videos" do
      user = Factory.insert(:user)

      videos = Multimedia.list_user_videos(user)

      assert videos == []
    end

    test "list_user_videos returns a list of videos owned by the user" do
      user = Factory.insert(:user)
      _video = Factory.insert(:video, user: user)

      videos = Multimedia.list_user_videos(user)

      assert Enum.count(videos) == 1
    end

    test "list_user_videos excludes videos owned by other users" do
      bob = Factory.insert(:user)
      Factory.insert(:video, user: bob)
      alice = Factory.insert(:user)

      alice_videos = Multimedia.list_user_videos(alice)
      bob_videos = Multimedia.list_user_videos(bob)

      assert Enum.count(alice_videos) == 0
      assert Enum.count(bob_videos) == 1
    end

    test "get_user_video! returns a video owned by the user" do
      user = Factory.insert(:user)
      video = Factory.insert(:video, user: user)

      result = Multimedia.get_user_video!(user, video.id)

      assert video.id == result.id
      assert video.title == result.title
      assert video.description == result.description
    end

    test "get_user_video! does not allow a user to see another user's video" do
      alice = Factory.insert(:user)
      bob = Factory.insert(:user)
      bobs_video = Factory.insert(:video, user: bob)

      assert_raise Ecto.NoResultsError, fn ->
        Multimedia.get_user_video!(alice, bobs_video.id)
      end
    end
  end

  describe "annotations" do
    setup do
      user = Factory.insert(:user)
      video =
        Factory.insert(:video, user: user)
        |> Ecto.reset_fields([:user])

      {:ok, video: video, user: user}
    end

    test "create annotation with valid data", %{video: video, user: user} do
      {:ok, result} = Multimedia.annotate_video(user, video.id, %{body: "the message body", at: 1234})

      assert %Multimedia.Annotation{} = result
      assert "the message body" == result.body
      assert 1234 == result.at
      assert user.id == result.user_id
      assert video.id == result.video_id
    end

    test "list annotations for a given video and user ordered by video timecode", %{video: video, user: user} do
      {:ok, first_annotation} = Multimedia.annotate_video(user, video.id, %{body: "Hello world", at: 1234})
      {:ok, second_annotation} = Multimedia.annotate_video(user, video.id, %{body: "Goodbye world", at: 4567})
      # Creating annotations does not preload the User relation. But the list_annotation function does.
      # Massage the fixture data to mimic what list_annotations should return.
      [first_annotation, second_annotation] = Repo.preload([first_annotation, second_annotation], :user)

      results = Multimedia.list_annotations(video)

      assert [first_annotation, second_annotation] == results
    end

    test "list annotations since ID", %{video: video, user: user} do
      {:ok, first_annotation} = Multimedia.annotate_video(user, video.id, %{body: "Hello world", at: 1234})
      {:ok, second_annotation} = Multimedia.annotate_video(user, video.id, %{body: "Goodbye world", at: 4567})
      [first_annotation, second_annotation] = Repo.preload([first_annotation, second_annotation], :user)

      results = Multimedia.list_annotations(video, first_annotation.id)

      assert [second_annotation] == results
    end
  end
end
