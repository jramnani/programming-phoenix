defmodule Rumbl.Accounts.AccountsTest do
  use Rumbl.DataCase, async: true

  alias Rumbl.Accounts
  alias Rumbl.Accounts.User

  describe "find users" do
    setup do
      Repo.insert!(%User{name: "Jose", username: "josevalim"})
      Repo.insert!(%User{name: "Chris", username: "chrismccord"})
      Repo.insert!(%User{name: "Bruce", username: "redrapids"})

      :ok
    end

    test "get user by ID - exists" do
      first_user = List.first(Repo.all(User))

      actual_user = Accounts.get_user(first_user.id)

      assert first_user == actual_user
    end

    test "get user by ID - does not exist" do
      result = Accounts.get_user("100")

      assert nil == result
    end

    test "get user by params - exists" do
      params = %{
        name: "Chris",
        username: "chrismccord"
      }

      result = Accounts.get_user_by(params)

      assert "Chris" == result.name
      assert "chrismccord" == result.username
    end

    test "get user by params - does not exist" do
      params = %{
        name: "Bogus",
        username: "bogus"
      }

      result = Accounts.get_user_by(params)

      assert nil == result
    end

    test "get user by params - one attribute" do
      result = Accounts.get_user_by(username: "chrismccord")

      assert result.name == "Chris"
      assert result.username == "chrismccord"
    end

    test "list users by IDs" do
      alice = Factory.insert(:user, username: "alice")
      bob = Factory.insert(:user, username: "bob")
      mary = Factory.insert(:user, username: "mary")

      result = Accounts.list_users_by_ids([alice.id, bob.id])
      result_ids = Enum.map(result, &(&1.id))

      assert alice.id in result_ids
      assert bob.id in result_ids
      assert mary.id not in result_ids
    end
  end

  describe "register users" do

    test "with valid data then insert user into the database" do
      user_params = %{
        name: "Test User",
        username: "testuser",
        password: "supersecret"
      }

      assert {:ok, %User{id: id} = user} = Accounts.register_user(user_params)
      assert user.name == "Test User"
      assert user.username == "testuser"
      assert [%User{id: ^id}] = Accounts.list_users()
    end

    test "with invalid data then do not insert into database" do
      invalid_user_params = %{
        name: nil,
        username: nil,
        password: nil
      }

      assert {:error, _changeset} = Accounts.register_user(invalid_user_params)
      assert Accounts.list_users() == []
    end

    test "enforces unique usernames" do
      user_params = %{
        name: "Test User",
        username: "testuser",
        password: "supersecret"
      }

      assert {:ok, %User{}} = Accounts.register_user(user_params)

      assert {:error, changeset} = Accounts.register_user(user_params)
      assert %{username: ["has already been taken"]} == errors_on(changeset)
    end

    test "enforces max length on usernames" do
      user_params = %{
        name: "Test User",
        username: String.duplicate("z", 30),
        password: "supersecret"
      }

      {:error, changeset} = Accounts.register_user(user_params)
      %{username: [reason]} = errors_on(changeset)
      assert String.contains?(reason, "should be at most")
    end

    test "enforces password to be at least 6 characters long" do
      user_params = %{
        name: "Test User",
        username: "testuser",
        password: "12345"
      }

      {:error, changeset} = Accounts.register_user(user_params)
      %{password: [reason]} = errors_on(changeset)
      assert String.contains?(reason, "should be at least")
    end
  end

  describe "authenticate users" do

    test "authenticate user with valid username and password" do
      {:ok, user} =
        Accounts.register_user(%{
          username: "phoenix",
          name: "Phoenix",
          password: "secret"
        })

      {status, payload} = Accounts.authenticate_user("phoenix", "secret")

      assert status == :ok
      assert payload.id == user.id
    end

    test "authenticate user with invalid password fails" do
      {:ok, _user} =
        Accounts.register_user(%{
          username: "phoenix",
          name: "Phoenix",
          password: "secret"
        })

      {status, payload} = Accounts.authenticate_user("phoenix", "BadPassword")

      assert status == :error
      assert payload == :unauthorized
    end

    test "authenticate user with no matching user fails" do
      {:ok, _user} =
        Accounts.register_user(%{
          username: "phoenix",
          name: "Phoenix",
          password: "secret"
        })

      {status, payload} = Accounts.authenticate_user("DoesNotExist", "123456")

      assert status == :error
      assert payload == :not_found
    end
  end
end
