defmodule RumblWeb.Controllers.Auth do
  use RumblWeb.ConnCase, async: true

  alias Rumbl.Accounts
  alias RumblWeb.Auth

  describe "Auth Plug Module" do
    test "When user_id in session then add user to assigns" do
      Factory.insert(:user, username: "josevalim")
      expected_user = Accounts.get_user_by(username: "josevalim")

      conn =
        build_conn()
        |> Plug.Test.init_test_session(user_id: expected_user.id)
        |> Auth.call(Auth.init([]))

      assert conn.assigns.current_user == expected_user
    end

    test "When user_id is invalid then current_user is nil" do
      user = Factory.insert(:user)

      conn =
        build_conn()
        # Compute a user ID that doesn't not exist.
        |> Plug.Test.init_test_session(user_id: user.id + 1)
        |> Auth.call(Auth.init([]))

      assert conn.assigns.current_user == nil
    end

    test "When no user_id in session then current_user is nil" do
      conn =
        build_conn()
        |> Plug.Test.init_test_session(foo: "bar")
        |> Auth.call(Auth.init([]))

      assert conn.assigns.current_user == nil
    end
  end

  describe "Auth Plug Functions" do
    setup %{conn: conn} do
      # Call bypass_through to do a request that goes through the whole
      # pipeline but bypasses the router dispatch.
      #
      # Perform a request with get(), which accesses the endpoint and
      # stops at the browser pipeline, as requested.
      # The path given to "get" isn't used by the router when bypassing;
      # it is simply stored in the connection.
      #
      # This gives us all the requirements for a plug with a valid session and
      # flash message support.
      # Without this technqiue we would need to call
      # `Plug.Test.init_test_session()` and Plug.fetch_session() to initialize
      # those two features and avoid errors.

      # Apply this technique to the Plug.Conn created by ConnCase and passed
      # into this test setup.
      conn =
        conn
        |> bypass_through(RumblWeb.Router, :browser)
        |> get("/")

      {:ok, %{conn: conn}}
    end

    test "authenticated_user halts when no current_user in assigns", %{conn: conn} do
      conn = Auth.authenticate_user(conn, %{})

      assert conn.halted
    end

    test "authenticate_user allows when current_user in assigns", %{conn: conn} do
      user = Factory.insert(:user)
      conn = assign(conn, :current_user, user)

      conn = Auth.authenticate_user(conn, %{})

      refute conn.halted
    end

    test "login puts the user in the session", %{conn: conn} do
      login_conn =
        conn
        |> Auth.login(%Accounts.User{id: 123})
        |> send_resp(:ok, "")

      next_conn = get(login_conn, "/")

      assert get_session(next_conn, :user_id) == 123
    end

    test "logout drops the session", %{conn: conn} do
      logout_conn =
        conn
        |> put_session(:user_id, 123)
        |> Auth.logout()
        |> send_resp(:ok, "")

      next_conn = get(logout_conn, "/")

      refute get_session(next_conn, :user_id)
    end
  end
end
