defmodule RumblWeb.VideoControllerTest do
  use RumblWeb.ConnCase

  @create_attrs %{description: "some description", title: "some title", url: "some url"}
  @update_attrs %{description: "some updated description", title: "some updated title", url: "some updated url"}
  @invalid_attrs %{description: nil, title: nil, url: nil}

  setup do
    user = Factory.insert(:user)
    conn = build_conn_login_as(user)

    {:ok, conn: conn, user: user}
  end

  describe "authorization" do
    test "prevents actions on videos by non-owners" do
      owner = Factory.insert(:user, username: "owner")
      owner_video = Factory.insert(:video, user: owner)
      attacker = Factory.insert(:user, username: "attacker")

      # Log in as the attacker
      conn = build_conn_login_as(attacker)

      assert_error_sent :not_found, fn ->
        get(conn, ~p"/manage/videos/#{owner_video}")
      end

      assert_error_sent :not_found, fn ->
        get(conn, ~p"/manage/videos/#{owner_video}/edit")
      end

      assert_error_sent :not_found, fn ->
        put(conn, ~p"/manage/videos/#{owner_video}", video: @create_attrs)
      end

      assert_error_sent :not_found, fn ->
        delete(conn, ~p"/manage/videos/#{owner_video}")
      end
    end
  end

  describe "index" do
    test "lists all videos", %{conn: conn} do
      conn = get(conn, ~p"/manage/videos")

      assert html_response(conn, 200) =~ "Listing Videos"
    end
  end

  describe "new video" do
    test "renders form", %{conn: conn} do
      conn = get(conn, ~p"/manage/videos/new")
      assert html_response(conn, 200) =~ "New Video"
    end
  end

  describe "create video" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, ~p"/manage/videos", video: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == ~p"/manage/videos/#{id}"

      conn = get(conn, ~p"/manage/videos/#{id}")
      {numerical_id, _rest_of_slug} = Integer.parse(id)
      assert html_response(conn, 200) =~ "Video #{numerical_id}"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, ~p"/manage/videos", video: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Video"
    end
  end

  describe "edit video" do
    setup [:create_video]

    test "renders form for editing chosen video", %{conn: conn, video: video} do
      conn = get(conn, ~p"/manage/videos/#{video}/edit")
      assert html_response(conn, 200) =~ "Edit Video"
    end
  end

  describe "update video" do
    setup [:create_video]

    test "redirects when data is valid", %{conn: conn, video: video} do
      conn = put(conn, ~p"/manage/videos/#{video}", video: @update_attrs)
      # Loosen this assertion because the Verified Route on the right hand side
      # is not including the title slug in the path, and I'm not sure why.
      assert redirected_to(conn) =~ ~p"/manage/videos/#{video}"

      conn = get(conn, ~p"/manage/videos/#{video}")
      assert html_response(conn, 200) =~ "some updated description"
    end

    test "renders errors when data is invalid", %{conn: conn, video: video} do
      conn = put(conn, ~p"/manage/videos/#{video}", video: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Video"
    end
  end

  describe "delete video" do
    setup [:create_video]

    test "deletes chosen video", %{conn: conn, video: video} do
      conn = delete(conn, ~p"/manage/videos/#{video}")
      assert redirected_to(conn) == ~p"/manage/videos"

      assert_error_sent 404, fn ->
        get(conn, ~p"/manage/videos/#{video}")
      end
    end
  end

  # setup function helpers get a Map of state accumulated by each test setup.
  # Print out the function arg to see all the data available for setup.
  defp create_video(%{user: user}) do
    video = Factory.insert(:video, user: user)
    %{video: video}
  end
end
