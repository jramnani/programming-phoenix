defmodule RumblWeb.UserControllerTest do
  use RumblWeb.ConnCase, async: true

  alias Rumbl.Repo
  alias Rumbl.Accounts.User

  setup do
    Repo.insert!(%User{name: "Jose", username: "josevalim"})
    Repo.insert!(%User{name: "Chris", username: "chrismccord"})
    Repo.insert!(%User{name: "Bruce", username: "redrapids"})

    :ok
  end

  test "GET /users" do
    logged_in_user = Repo.get_by(User, username: "josevalim")

    conn =
      build_conn()
      |> Plug.Test.init_test_session(user_id: logged_in_user.id)
      |> RumblWeb.Auth.login(logged_in_user)

    conn = get(conn, ~p"/users")

    assert html_response(conn, 200) =~ "Jose"
    assert html_response(conn, 200) =~ "josevalim"
  end

  test "GET /users/:id" do
    expected_user = Repo.get_by(User, username: "josevalim")
    assert expected_user != nil

    conn =
      build_conn()
      |> Plug.Test.init_test_session(user_id: expected_user.id)
      |> RumblWeb.Auth.login(expected_user)

    conn = get(conn, ~p"/users/#{expected_user.id}")

    assert html_response(conn, 200) =~ expected_user.name
  end

  test "GET /users/new", %{conn: conn} do
    conn = get(conn, ~p"/users/new")

    assert html_response(conn, 200) =~ "New User"
  end

  test "POST /users - valid params", %{conn: conn} do
    new_user_params = %{
      user: %{
        name: "Max",
        username: "max",
        password: "secret123"
      }
    }

    conn = post(conn, ~p"/users", new_user_params)

    assert conn.assigns.flash == %{"info" => "Max created!"}
  end

  test "POST /users - invalid params", %{conn: conn} do
    new_user_params = %{
      user: %{
        name: "Max",
        username: "max",
        # password is too short
        password: "123"
      }
    }

    conn = post(conn, ~p"/users", new_user_params)

    assert html_response(conn, 200) =~ "should be at least 6 character(s)"
  end
end
