# What is this?

This repository contains my working examples from the book [Programming Phoenix 1.4],
for a newer version of Phoenix. In my case Phoenix version 1.7.

There have been a lot of changes to the View layer of the framework between 1.4
and 1.7. This repository contains working examples using the new View layer with
[Phoenix.Component].

My style is a little different from the authors of the book, but hopefully the
examples are close enough to the book to help you get unstuck if you come across
an error.

Content starting on Chapter 11 is on a different branch named, `umbrella`. The
refactoring from a vanilla Phoenix application to an umbrella-style project was
big enough to be separated from the majority of the examples presented in the
book.

# Rumbl

To start your Phoenix server:

  * Run `mix setup` to install and setup dependencies
  * Start Phoenix endpoint with `mix phx.server` or inside IEx with `iex -S mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## Learn more

  * Official website: https://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Forum: https://elixirforum.com/c/phoenix-forum
  * Source: https://github.com/phoenixframework/phoenix

[Programming Phoenix 1.4]: https://pragprog.com/titles/phoenix14/programming-phoenix-1-4/
[Phoenix.Component]: https://hexdocs.pm/phoenix/components.html
