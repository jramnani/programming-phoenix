;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((elixir-mode . (
                 (elixir-ls-path . "$HOME/code/elixir-ls/release")
                 (elixir-backend . lsp)
                 )
))
